#!/usr/bin/env python3

import encode_single_video

import os

import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="")

    parser.add_argument(
        "-o",
        "--output-dir",
        required=False,
        type=str,
        help='Output directory for encodes. Default is "encodes" in current working directory',
        default="encodes",
    )

    parser.add_argument(
        "-k",
        "--keep-encodes",
        action="store_true",
        required=False,
        help="Don't delete encodes after getting their scores. Default is False (delete)",
    )

    args = parser.parse_args()

    # REVIEW expose this as variable/argument?
    outputpath: str = args.output_dir

    keepencodes: bool = args.keep_encodes

    # tuple of video file extensions
    videofileextensions = (".webm", ".mp4")

    # search all video files in "source"
    videofiles: list[str] = []
    for root, dirs, files in os.walk("source"):
        for file in files:
            if file.endswith(videofileextensions):
                filepath = os.path.join(root, file)
                videofiles.append(filepath)

    try:
        # encode all video files
        for video in videofiles:
            encode_single_video.main(
                inputfile=video, outputpath=outputpath, keepencodes=keepencodes
            )
    except KeyboardInterrupt:
        encode_single_video.cleanup(keepencodes=keepencodes)
