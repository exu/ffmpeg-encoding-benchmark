#!/usr/bin/env python3

import os

import csv

from typing import Any

# file = "/home/marc/Dokumente/v0.0.1a2-data-1666552071.csv"
file = r"C:\Users\marc.friemelt\Nextcloud\Dokumente\v0.0.1a2-data-1666552071.csv"

content = {}

# Create dict from csv file
# Key names are the ones used in the csv file
with open(file, "r") as f:
    reader = csv.reader(f)
    headers = next(reader)
    for row in reader:
        codec_preset = " ".join([row[0], row[2]])
        try:
            content[codec_preset]
        except KeyError:
            content[codec_preset] = []
        # NOTE | to merge dicts requires python 3.9 or newer
        content[codec_preset].append(
            {"CRF": row[1]}
            | {"Input file": row[3]}
            | {"Output file size (MiB)": row[4]}
            | {"Encode time (s)": row[5]}
            | {"VMAF Score (mean)": row[6]}
            | {"VMAF Score (min)": row[7]}
        )

print(content)

exit()

import plotly.graph_objects as go
import plotly.express as px

# Load data, define hover text and bubble size
data = px.data.gapminder()
df_2007 = data[data["year"] == 2007]
df_2007 = df_2007.sort_values(["continent", "country"])

"""
hover_text = []
bubble_size = []

for index, row in df_2007.iterrows():
    hover_text.append(
        (
            "Country: {country}<br>"
            + "Life Expectancy: {lifeExp}<br>"
            + "GDP per capita: {gdp}<br>"
            + "Population: {pop}<br>"
            + "Year: {year}"
        ).format(
            country=row["country"],
            lifeExp=row["lifeExp"],
            gdp=row["gdpPercap"],
            pop=row["pop"],
            year=row["year"],
        )
    )
    bubble_size.append(math.sqrt(row["pop"]))

df_2007["text"] = hover_text
df_2007["size"] = bubble_size
sizeref = 2.0 * max(df_2007["size"]) / (100**2)
"""

# Dictionary with dataframes for each continent
continent_names = ["Africa", "Americas", "Asia", "Europe", "Oceania"]
continent_data = {
    continent: df_2007.query("continent == '%s'" % continent)
    for continent in continent_names
}

# Create figure
fig = go.Figure()

for continent_name, continent in continent_data.items():
    fig.add_trace(
        go.Scatter(
            x=continent["gdpPercap"],
            y=continent["lifeExp"],
            name=continent_name,
            # text=continent["text"],
            # marker_size=continent["size"],
        )
    )

# Tune marker appearance and layout
fig.update_traces(
    # mode="markers", marker=dict(sizemode="area", sizeref=sizeref, line_width=2)
    mode="markers"
)

fig.update_layout(
    title="Graph title",
    xaxis=dict(
        title="X-Axis",
        gridcolor="white",
        # type="log",
        gridwidth=2,
    ),
    yaxis=dict(
        title="Y-Axis",
        gridcolor="white",
        gridwidth=2,
    ),
    # paper_bgcolor="rgb(243, 243, 243)",
    # plot_bgcolor="rgb(243, 243, 243)",
)
# fig.show()

fig = go.Figure()

# NOTE explicit convertion to integer necessary
max_x = max(int(line["Encode time (s)"]) for line in content)
max_y = max(float(line["Output file size (MiB)"]) for line in content)

x_axis = list(line["Encode time (s)"] for line in content)
y_axis = list(line["Output file size (MiB)"] for line in content)

for line in content:
    fig.add_trace(
        go.Scatter(
            x=x_axis,
            # x=list(range(0, max_x)),
            # x=line["Encode time (s)"],
            y=y_axis,
            # y=list(range(0, int(max_y))),
            # y=line["Output file size (MiB)"],
            name=line["Codec Preset"],
        )
    )

fig.update_traces(mode="markers")

fig.update_layout(
    title="Graph title",
    xaxis=dict(
        title="X-Axis",
        gridcolor="white",
        # type="log",
        gridwidth=2,
    ),
    yaxis=dict(
        title="Y-Axis",
        gridcolor="white",
        gridwidth=2,
    ),
)

fig.show()
