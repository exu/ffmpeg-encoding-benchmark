#!/usr/bin/env python3

import os

import csv

file = "/home/marc/Dokumente/v0.0.1a2-data-1666552071.csv"

with open(file, "r") as f:
    reader = csv.reader(f)
    output = os.path.splitext(file)[0] + "-joined.csv"
    with open(output, "w") as file:
        writer = csv.writer(file)
        for row in reader:
            new_row = [" ".join([row[0], row[2]])] + [row[1]] + row[3:]
            writer.writerow(new_row)
