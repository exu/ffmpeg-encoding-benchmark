# Notes
`ffmpeg -i almatrailer2016a.mp4 -map 0:v:0`  
`ffmpeg -i eso1902a.mp4 -map 0:v:0`  
`ffmpeg -i Sparks_in_Blender.webm -map 0:v:0`  

```
$crf: CRF, see encoder documentation for valid values
$preset: Encoder preset, see documentation for values
$keyint: Keyframe interval. Set to (fps * 10) for consistency
```

## H.264
`-c:v libx264 -crf $crf -preset $preset -g $keyint`  

CRF: 0–51  

Presets:  
```
ultrafast
superfast
veryfast
faster
fast
medium
slow
slower
veryslow
placebo
```

## H.265
`-c:v libx265 -crf $crf -preset $preset -g $keyint`  

CRF: 0-51  

Presets:  
```
ultrafast
superfast
veryfast
faster
fast
medium
slow
slower
veryslow
placebo
```

## AV1
### libaom
`-c:v libaom-av1 -crf $crf -b:v 0 -cpu-used $preset -row-mt 1 -g $keyint -tiles 2x2`  

CRF: 0-63  

Presets: 0-6  

### SVT-AV1
`-c:v libsvtav1 -crf $crf -preset $preset -g $keyint`  

CRF: 0-63  

Presets: 0-12  
