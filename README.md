# FFMPEG Video Encoding benchmarks
[![status-badge](https://woodpecker.exu.li/api/badges/RealStickman/ffmpeg-encoding-benchmark/status.svg)](https://woodpecker.exu.li/RealStickman/ffmpeg-encoding-benchmark)  

## Attribution
Sparks_in_Blender.webm: [Sparks in Blender](https://www.youtube.com/watch?v=MGRhJf0xdE8) by [Blender Guru](https://www.youtube.com/c/BlenderGuruOfficial) licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)  
eso1902a.mp4: [ESOcast 191 Light: A Fleeting Moment in Time](https://www.eso.org/public/videos/eso1902a/) by [ESO](https://www.eso.org/public/) licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)  
almatrailer2016a.mp4: [ALMA trailer](https://www.eso.org/public/videos/almatrailer2016a/) by [ESO](https://www.eso.org/public/) licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)  

## Resources
[My own notes on FFMPEG](https://wiki.exu.li/en/other/ffmpeg)  

[Using SVT-AV1 within ffmpeg](https://gitlab.com/AOMediaCodec/SVT-AV1/-/blob/master/Docs/Ffmpeg.md)  

## Video Quality
Some resources on video quality, the VMAF, SSIM and MSE metrics and how they stack up to subjective perception.  

### Stuff I need to check out
https://streaminglearningcenter.com/learning/mapping-ssim-vmaf-scores-subjective-ratings.html  
https://streaminglearningcenter.com/encoding/best-practices-for-netflixs-vmaf-metric.html  
https://medium.com/netflix-techblog/vmaf-the-journey-continues-44b51ee9ed12  
https://streaminglearningcenter.com/blogs/vmaf-is-hackable-what-now.html  
https://www.reddit.com/r/ffmpeg/comments/qt01wf/good_vmaf_values_to_target_for_a_reencode/  
https://audiovideotestlab.com/blog/full-reference-quality-metrics-vmaf-psnr-and-ssim/  

