# Media Info
Slightly redacted media info for sample video files  

## almatrailer2016a.mp4
```
General
Format                                   : MPEG-4
Format profile                           : Base Media / Version 2
Codec ID                                 : mp42 (mp42/mp41)
File size                                : 727 MiB
Duration                                 : 3 min 19 s
Overall bit rate mode                    : Variable
Overall bit rate                         : 30.6 Mb/s
Encoded date                             : UTC 2016-12-02 10:49:17
Tagged date                              : UTC 2016-12-02 10:49:31
TIM                                      : 00:00:00:00
TSC                                      : 25
TSZ                                      : 1

Video
ID                                       : 1
Format                                   : AVC
Format/Info                              : Advanced Video Codec
Format profile                           : High@L5.1
Format settings                          : CABAC / 3 Ref Frames
Format settings, CABAC                   : Yes
Format settings, Reference frames        : 3 frames
Codec ID                                 : avc1
Codec ID/Info                            : Advanced Video Coding
Duration                                 : 3 min 19 s
Bit rate mode                            : Variable
Bit rate                                 : 30.4 Mb/s
Maximum bit rate                         : 35.0 Mb/s
Width                                    : 3 840 pixels
Height                                   : 2 160 pixels
Display aspect ratio                     : 16:9
Frame rate mode                          : Constant
Frame rate                               : 25.000 FPS
Standard                                 : PAL
Color space                              : YUV
Chroma subsampling                       : 4:2:0
Bit depth                                : 8 bits
Scan type                                : Progressive
Bits/(Pixel*Frame)                       : 0.147
Stream size                              : 721 MiB (99%)
Language                                 : English
Encoded date                             : UTC 2016-12-02 10:49:17
Tagged date                              : UTC 2016-12-02 10:49:17
Color range                              : Limited
Color primaries                          : BT.709
Transfer characteristics                 : BT.709
Matrix coefficients                      : BT.709
Codec configuration box                  : avcC

Audio
ID                                       : 2
Format                                   : AAC LC
Format/Info                              : Advanced Audio Codec Low Complexity
Codec ID                                 : mp4a-40-2
Duration                                 : 3 min 19 s
Source duration                          : 3 min 19 s
Bit rate mode                            : Variable
Bit rate                                 : 221 kb/s
Maximum bit rate                         : 324 kb/s
Channel(s)                               : 2 channels
Channel layout                           : L R
Sampling rate                            : 48.0 kHz
Frame rate                               : 46.875 FPS (1024 SPF)
Compression mode                         : Lossy
Stream size                              : 5.25 MiB (1%)
Source stream size                       : 5.25 MiB (1%)
Language                                 : English
Encoded date                             : UTC 2016-12-02 10:49:17
Tagged date                              : UTC 2016-12-02 10:49:17
```

## eso1902a.mp4
```
General
Format                                   : MPEG-4
Format profile                           : Base Media / Version 2
Codec ID                                 : mp42 (mp42/mp41)
File size                                : 285 MiB
Duration                                 : 1 min 26 s
Overall bit rate mode                    : Variable
Overall bit rate                         : 27.6 Mb/s
Encoded date                             : UTC 2019-01-17 16:02:49
Tagged date                              : UTC 2019-01-17 16:02:57
TIM                                      : 00:00:00:00
TSC                                      : 25
TSZ                                      : 1

Video
ID                                       : 1
Format                                   : HEVC
Format/Info                              : High Efficiency Video Coding
Format profile                           : Main@L5.2@Main
Codec ID                                 : hvc1
Codec ID/Info                            : High Efficiency Video Coding
Duration                                 : 1 min 26 s
Bit rate                                 : 27.3 Mb/s
Width                                    : 3 840 pixels
Height                                   : 2 160 pixels
Display aspect ratio                     : 16:9
Frame rate mode                          : Constant
Frame rate                               : 25.000 FPS
Color space                              : YUV
Chroma subsampling                       : 4:2:0
Bit depth                                : 8 bits
Bits/(Pixel*Frame)                       : 0.132
Stream size                              : 282 MiB (99%)
Language                                 : English
Encoded date                             : UTC 2019-01-17 16:02:49
Tagged date                              : UTC 2019-01-17 16:02:49
Color range                              : Full
Color primaries                          : BT.709
Transfer characteristics                 : BT.709
Matrix coefficients                      : BT.709
Codec configuration box                  : hvcC

Audio
ID                                       : 2
Format                                   : AAC LC
Format/Info                              : Advanced Audio Codec Low Complexity
Codec ID                                 : mp4a-40-2
Duration                                 : 1 min 26 s
Source duration                          : 1 min 26 s
Bit rate mode                            : Variable
Bit rate                                 : 221 kb/s
Maximum bit rate                         : 314 kb/s
Channel(s)                               : 2 channels
Channel layout                           : L R
Sampling rate                            : 48.0 kHz
Frame rate                               : 46.875 FPS (1024 SPF)
Compression mode                         : Lossy
Stream size                              : 2.29 MiB (1%)
Source stream size                       : 2.29 MiB (1%)
Language                                 : English
Encoded date                             : UTC 2019-01-17 16:02:49
Tagged date                              : UTC 2019-01-17 16:02:49
```

## Sparks_in_Blender.webm
```
General
Format                                   : WebM
Format version                           : Version 2
File size                                : 17.5 MiB
Duration                                 : 47 s 334 ms
Overall bit rate                         : 3 096 kb/s
Writing application                      : Lavf57.25.100
Writing library                          : Lavf57.25.100

Video
ID                                       : 1
Format                                   : VP8
Codec ID                                 : V_VP8
Duration                                 : 47 s 333 ms
Bit rate                                 : 2 966 kb/s
Width                                    : 1 920 pixels
Height                                   : 1 080 pixels
Display aspect ratio                     : 16:9
Frame rate mode                          : Constant
Frame rate                               : 24.000 FPS
Compression mode                         : Lossy
Bits/(Pixel*Frame)                       : 0.060
Stream size                              : 16.7 MiB (96%)
Default                                  : Yes
Forced                                   : No
```
